---
layout: job_family_page
title: "Threat Management Roles"
description: "As part of the Threat Management sub-department, the vulnerability management team is responsible for understanding our cloud footprint and any potential vulnerabilities"
extra_js:
- libs/mermaid.min.js
---

As part of the Threat Management sub-department, the vulnerability management team is responsible for understanding our cloud footprint and any potential vulnerabilities. We will accomplish this mission by working closely with infrastructure security, infrastructure and other security department teams.

## Team Roles and Responsibilities
- Own and execute on a cloud asset management strategy.
- Own and execute on a vulnerability management strategy across all GitLab cloud environments. 
- Own and execute on a patch management strategy collaborating with other teams as necessary. 
- Define a roadmap to continually assess and iterate security best practices for our cloud environments.
- Support IT, infrastructure and infrastructure security efforts where possible
- Ensure the protection of both GitLab and GitLab customer data
- Track new and emerging threats to our environments

## Manager, Vulnerability Management Team
This position reports to Sr. Manager of Threat Management
Initially this position will be “player/coach” and play a role in building their team.
As with other manager roles in security department this is a grade 8 

## Responsibilities
- Define and iterate a department strategy and direction that addresses the following:
- Multi-cloud asset management
- Multi-cloud vulnerability management
- Multi-cloud patch management
- Define team structure, technical needs and job descriptions.
- Hiring
- Career Development
- Project Management
- Team Retrospectives
- Development department metrics
- Collaborate across all GitLab departments as necessary to further team strategy and goal.
- Identify, document and track team quarterly goals (OKRs).
- Provide tactical oversight and direction to the team.
- Hold regular 1:1 and team meetings.
- Ensure project plans and other documentation is always complete.
- Present on or perform read outs on initiative results to key stakeholders.
- Provide input and support to other managers across the Security Department. 
- Demonstrate GitLab values and lead by example.

## Qualifications
- Understanding of Git, and GitLab.
- Proven track record in executing on a comprehensive vulnerability management program.
- Hands on experience with all major cloud providers.
- Hands on experience with asset management, vulnerability management and patch management methodologies and tools. 
- Experience leading security teams.
- Experience with a SaaS company.
- Remote work experience. 
- Robust sense of ownership, urgency, and drive
- Excellent written and verbal communication skills, especially experience with executive-level communications
- Capability to make sound decisions in the face of ambiguity and imperfect knowledge
- Share our values, and work in accordance with those values
- Alignment with Manager responsibilities as outlined in Leadership at GitLab

## Hiring Process
Candidates for the Vulnerability Management Team Manager can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.
Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters.
Next, candidates will be invited to schedule a 45-60 minute interview with the Director of Threat Management.
Candidates will then be invited to schedule separate 45 minute interviews with three members of the Security Organization
Candidates will then be invited to an 30 minute interview with the VP of Security
Successful candidates will subsequently be made an offer via email
Additional details about our process can be found on our hiring page.
